<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attacking_tile_id');
            $table->unsignedInteger('defending_tile_id');
            $table->integer('stage')->default(0);
            $table->string('winner')->nullable();
            $table->double('status', 15, 4)->default(10.000);

            $table->unsignedDecimal('troop_1_atk', 20, 8)->default(0);
            $table->unsignedDecimal('troop_2_atk', 20, 8)->default(0);
            $table->unsignedDecimal('troop_3_atk', 20, 8)->default(0);

            $table->unsignedInteger('defender_id')->index();
            $table->unsignedInteger('created_by')->default(2)->index();
            $table->unsignedInteger('updated_by')->default(2);
            $table->dateTime('next_stage_at')->nullable();
            $table->dateTime('battle_starts_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battles');
    }
}
