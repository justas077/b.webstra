<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapTilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_tiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('x');
            $table->integer('y');
            $table->string('type');
            $table->string('village_type')->nullable();
            $table->integer('village_level')->default(0);
            $table->unsignedDecimal('troop_1', 20, 8)->default(0);
            $table->unsignedDecimal('troop_2', 20, 8)->default(0);
            $table->unsignedDecimal('troop_3', 20, 8)->default(0);
            $table->unsignedDecimal('troop_4', 20, 8)->default(0);
            $table->unsignedDecimal('troop_5', 20, 8)->default(0);
            $table->string('produce_troop')->nullable();

            $table->dateTime('troops_at')->default(\Illuminate\Support\Facades\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
            $table->unsignedInteger('owner_id')->default(2)->index();
            $table->unsignedInteger('created_by')->default(2);
            $table->unsignedInteger('updated_by')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_tiles');
    }
}
