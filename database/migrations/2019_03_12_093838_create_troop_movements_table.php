<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTroopMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('troop_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('from_tile_id');
            $table->unsignedInteger('to_tile_id');
            $table->string('type')->nullable();
            $table->boolean('has_arrived')->default(false);
            $table->unsignedDecimal('troop_1', 20, 8)->default(0);
            $table->unsignedDecimal('troop_2', 20, 8)->default(0);
            $table->unsignedDecimal('troop_3', 20, 8)->default(0);

            $table->dateTime('will_arrive_at')->nullable();
            $table->unsignedInteger('created_by')->default(2)->index();
            $table->unsignedInteger('updated_by')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('troop_movements');
    }
}
