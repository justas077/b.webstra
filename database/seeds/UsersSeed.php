<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $system_user_id = DB::table('users')->insertGetId([
            'username' => 'system',
            'email' => 'system@ktu.lt',
            'password' => bcrypt(str_random(64)),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'created_by' => 1,
            'updated_by' => 1
        ]);

        DB::table('users')->insert([
            'username' => 'nature',
            'email' => 'nature@system.com',
            'password' => bcrypt(str_random(64)),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'created_by' => $system_user_id,
            'updated_by' => $system_user_id
        ]);

        DB::table('users')->insert([
            'username' => 'angrychild',
            'email' => 'angrychild@ktu.lt',
            'password' => bcrypt(!empty(env('MAIN_PASS'))?env('MAIN_PASS'):'abc123'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'created_by' => $system_user_id,
            'updated_by' => $system_user_id
        ]);

        DB::table('users')->insert([
            'username' => 'justinianas',
            'email' => 'justinianas@ktu.lt',
            'password' => bcrypt(!empty(env('MAIN_PASS'))?env('MAIN_PASS'):'abc123'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            'created_by' => $system_user_id,
            'updated_by' => $system_user_id
        ]);

    }
}
