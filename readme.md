# Webstra backend

## Instalation

### Requirements

Requirements for backend server:

- php >= 7.2
- composer ([site][composer-website])
- nginix or apache2
- mySQL


### Commands

1. Go to project folder.
2. Run `composer update`.
3. Copy environment file: `cp .env.example .env`.
4. Change environment variables.
5. Generate application key: `php artisan key:generate`.
6. Run database migrations: `php artisan migrate`
7. Generate map: `php artisan generate-map SIZE` (SIZE must be same as in .env file)

[composer-website]: https://getcomposer.org/