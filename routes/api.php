<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::prefix('user')->group(function () {
        Route::get('/', 'UserController@get');
        Route::get('/resources', 'UserController@resources');
    });

    Route::prefix('villages')->group(function () {
        Route::get('/{village}', 'VillagesController@get');
        Route::post('/{village}/construction', 'VillagesController@upgrade');
        Route::delete('/{village}', 'VillagesController@destroy');
    });

    Route::prefix('map')->group(function () {
        Route::put('chunks', 'MapController@getChunks');
        Route::get('current-user', 'MapController@currentUser');
    });

    Route::prefix('troops')->group(function () {
        Route::get('movements', 'TroopsController@index');
        Route::put('move/{from}/{to}', 'TroopsController@move');
        Route::put('attack/{from}/{to}', 'TroopsController@attack');
    });

    Route::prefix('battles')->group(function () {
        Route::get('/', 'BattlesController@index');
        Route::get('/{battle}', 'BattlesController@get');
    });

    Route::get('configuration', 'GenerateController@configuration');
});