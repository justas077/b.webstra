@servers(['staging' => 'webstra@18.224.93.114', 'production' => 'webstra@18.224.93.114'])

@setup
    $repository = 'git@gitlab.com:justas077/b.webstra.git';
    $app_dir = '/home/'.(($stage=='production')?'webstra':'webstra').'/b.webstra';
@endsetup

@story('deploy')
    pull_repository
    run_composer
    migrate
@endstory

@task('pull_repository', ['on'=>$stage])
    echo 'Pulling repository'
    cd {{ $app_dir }}
    git pull origin master
@endtask

@task('run_composer', ['on'=>$stage])
    echo "Installing dependencies"
    cd {{ $app_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('migrate', ['on'=>$stage])
    cd {{ $app_dir }}
    php artisan migrate --force --no-interaction;
@endtask
