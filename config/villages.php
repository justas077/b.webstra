<?php
return [

    "resources_basic_village" => 100,

    "factory" => [
        "name" => "Factory",
        //resources
        "level_bonus" => [
            "0" => 0,
            "1" => 200,
            "2" => 450,
            "3" => 1000
        ],
        "level_cost" => [
            //gold
            "1" => 200,
            "2" => 500,
            "3" => 1500
        ],
        "level_duration" => [
            //minutes
            "1" => 1,
            "2" => 2,
            "3" => 5
        ]
    ],

    "marketplace" => [
        "name" => "Marketplace",
        // + % for surrounding factories
        "level_bonus" => [
            "1" => 3,
            "2" => 5,
            "3" => 10
        ],
        "level_cost" => [
            //gold
            "1" => 1000,
            "2" => 5000,
            "3" => 20000
        ],
        "level_duration" => [
            //minutes
            "1" => 12,
            "2" => 46,
            "3" => 120
        ]
    ],

    "transport" => [
        "name" => "Transport",
        // - % travel time to/from this village
        "level_bonus" => [
            "1" => 60,
            "2" => 75,
            "3" => 90
        ],
        "level_cost" => [
            //gold
            "1" => 250,
            "2" => 500,
            "3" => 800
        ],
        "level_duration" => [
            //minutes
            "1" => 3,
            "2" => 6,
            "3" => 13
        ]
    ],

    "stronghold" => [
        "name" => "Stronghold",
        // % hp bonus for def
        "level_bonus" => [
            "1" => 3,
            "2" => 5,
            "3" => 10
        ],
        "level_cost" => [
            //gold
            "1" => 300,
            "2" => 900,
            "3" => 2500
        ],
        "level_duration" => [
            //minutes
            "1" => 5,
            "2" => 15,
            "3" => 35
        ]
    ],

    "siege" => [
        "name" => "Siege",
        // % attack bonus for atk
        "level_bonus" => [
            "1" => 3,
            "2" => 5,
            "3" => 10
        ],
        "level_cost" => [
            //gold
            "1" => 150,
            "2" => 250,
            "3" => 450
        ],
        "level_duration" => [
            //minutes
            "1" => 1,
            "2" => 4,
            "3" => 7
        ]
    ],

    "blacksmith" => [
        "name" => "Blacksmith",
        //surrounding villages can build other type troops
        "level_bonus" => [
            "1" => [ "troop_1" ],
            "2" => [ "troop_1", "troop_2" ],
            "3" => [ "troop_1", "troop_2", "troop_3" ]
        ],
        "level_cost" => [
            //gold
            "1" => 50,
            "2" => 500,
            "3" => 5000
        ],
        "level_duration" => [
            //minutes
            "1" => 3,
            "2" => 12,
            "3" => 50
        ]
    ],

    "barracks" => [
        "name" => "Barracks",
        // % faster build troop
        "level_bonus" => [
            "0" => 0,
            "1" => 40,
            "2" => 55,
            "3" => 80
        ],
        "level_cost" => [
            //gold
            "1" => 100,
            "2" => 400,
            "3" => 1000
        ],
        "level_duration" => [
            //minutes
            "1" => 2,
            "2" => 6,
            "3" => 15
        ]
    ]
];