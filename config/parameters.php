<?php
return [
    "map_size" => (int)env('MAP_SIZE'),
    "chunk_size" => (int)env('CHUNK_SIZE')
];