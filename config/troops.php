<?php

return [
    "troop_speed" => 1, //one tile - 20min
    "nature" => [
        "troop_1" => [
            "name" => "Powerful Duck",
            "tribe" => null,
            "power" => 200,
            "hp" => 300,
            "cost" => 2 // one troop - build 20 mins
        ],
        "troop_2" =>  [
            "name" => "Beautiful Bear",
            "tribe" => null,
            "power" => 35,
            "hp" => 120,
            "cost" => 25
        ],
        "troop_3" => [
            "name" => "Gray Wolf",
            "tribe" => null,
            "power" => 60,
            "hp" => 60,
            "cost" => 25
        ],
        "troop_4" => [
            "name" => "Long Snake",
            "tribe" => null,
            "power" => 20,
            "hp" => 200,
            "cost" => 25
        ],
        "troop_5" => [
            "name" => "Mega Turbo Cat",
            "tribe" => null,
            "power" => 100,
            "hp" => 100,
            "cost" => 20
        ]
    ],

    "player" => [
        "troop_1" => [
            "name" => "Armed peasant",
            "tribe" => null,
            "power" => 20,
            "hp" => 20,
            "cost" => 1
        ],
        "troop_2" => [
            "name" => "Cardinal",
            "tribe" => null,
            "power" => 200,
            "hp" => 50,
            "cost" => 2
        ],
        "troop_3" => [
            "name" => "Knight",
            "tribe" => null,
            "power" => 100,
            "hp" => 200,
            "cost" => 3
        ],
        "troop_4" => [
            "name" => "",
            "tribe" => null,
            "power" => 0,
            "hp" => 0,
            "cost" => 0
        ],
        "troop_5" => [
            "name" => "",
            "tribe" => null,
            "power" => 0,
            "hp" => 0,
            "cost" => 0
        ]
    ]
];
