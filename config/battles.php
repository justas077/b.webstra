<?php
return [
    "die_per_one_stage_per" => 10,
    "one_stage_duration_min" => 1,

    "one_stage_slider_divider" => 0.2, // kuo mazesnis, tuo greiciau auga
    "die_in_one_stage" => 0.02, // per 1 stage mirsta 2 proc visu kariu.

    "attack_effectiveness_drops_at" => 0.5,  // efektyvumas krenta jei puolantieji kariai nesudaro 50%
    "attack_penalty_multiply" => 2 // kuo didesnis, tuo nuobauda auga labiau
];