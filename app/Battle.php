<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Battle extends Model
{
    use BootableTrait;

    protected $fillable = [
        'attacking_tile_id',
        'defending_tile_id',
        'status',
        'troop_1_atk',
        'troop_2_atk',
        'troop_3_atk',
        'defender_id',
        'next_stage_at',
        'battle_starts_at',
        'winner',
        'stage'
    ];

    protected $hidden = [
        'updated_by',
        'updated_at',
        'created_at',
        'troop_1_atk',
        'troop_2_atk',
        'troop_3_atk',
        'defender'
    ];

    protected $appends = [
        "need_update_at",
        "attacking_troops",
        "defending_troops",
        "battle_in"
    ];

    public function defender()
    {
        return $this->hasOne(MapTile::class, 'id', 'defending_tile_id');
    }

    public function getBattleInAttribute()
    {
        return [
            "x" => $this->defender->x,
            "y" => $this->defender->y
        ];
    }

    public function allBackups()
    {
        return $this->hasMany(TroopMovement::class, 'to_tile_id', 'defending_tile_id')
                ->where('has_arrived', false)
                ->where(function($query) {
                    $query->where('type', 'backup')->orWhere('type', 'attack_backup');
                });
    }

    public function arrivedBackups()
    {
        return $this->allBackups()->where('will_arrive_at', '<=', Carbon::now());
    }

    public function notArrivedBackups()
    {
        return $this->allBackups()->where('will_arrive_at', '>', Carbon::now());
    }

    public function getNeedUpdateAtAttribute()
    {
        $time = (new Carbon($this->next_stage_at))->addMinutes(config('battles.one_stage_duration_min'))->addSeconds(10);
        if($this->defender_id == auth()->user()->id)
            $time = $time->addSeconds(5);
        return $time->toDateTimeString();
    }

    public function getAttackingTroopsAttribute()
    {
        $attackingTroops = [
            "troop_1" => $this->troop_1_atk,
            "troop_2" => $this->troop_2_atk,
            "troop_3" => $this->troop_3_atk
        ];

        if($this->winner != null && $this->defender_id == auth()->user()->id)
            return null;

        return $attackingTroops;
    }

    public function getDefendingTroopsAttribute()
    {
        $defendingTroops = [
            "troop_1" => $this->defender->troop_1,
            "troop_2" => $this->defender->troop_2,
            "troop_3" => $this->defender->troop_3
        ];

        if($this->defender_id == 2)
            $defendingTroops = array_merge($defendingTroops, [
                "troop_4" => $this->defender->troop_4,
                "troop_5" => $this->defender->troop_5,
            ]);

        if($this->winner != null && $this->created_by == auth()->user()->id)
            return null;

        return $defendingTroops;
    }

    public function battleMovement()
    {
        return $this->hasOne(TroopMovement::class, 'from_tile_id', 'attacking_tile_id');
    }
}
