<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapTileSimple extends Model
{
    protected $table = "map_tiles";

    protected $hidden = [
        "troop_1",
        "troop_2",
        "troop_3",
        "troop_4",
        "troop_5",
        "troops_at",
        "created_at",
        "updated_at",
        "created_by",
        "updated_by",
    ];
}
