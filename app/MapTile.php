<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Illuminate\Database\Eloquent\Model;

class MapTile extends Model
{
    use BootableTrait;

    const AVAILABLE_TYPES_PERCENTAGES = [
        //TOTAL 100
        'grassland' => 20,
        'fertile-land' => 15,
        'desert' => 15,
        'swamp' => 10,
        'forest' => 20,
        'hill' => 20
    ];

    const AVAILABLE_VILLAGES = [
        'stronghold',
        'barracks',
        'blacksmith',
        'factory',
        'marketplace',
        'transport',
        'siege'
    ];

    protected $fillable = [
        'type',
        'x',
        'y',
        'village_type',
        'updated_by',
        'village_level',
        'troop_1',
        'troop_2',
        'troop_3',
        'troop_4',
        'troop_5',
        'owner_id',
        'produce_troop',
        'troops_at'
    ];

    protected $hidden = [
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'troop_1',
        'troop_2',
        'troop_3',
        'troop_4',
        'troop_5',
        'owner_id'
    ];

    protected $appends = [
        'troops',
        'gold_per_hour',
        'troops_per_hour'
    ];

    protected $with = [
        'owner',
        'construction'
    ];

    public function getTroopsAttribute()
    {
        if($this->owner_id == auth()->user()->id)
            return [
                "troop_1" => $this->troop_1,
                "troop_2" => $this->troop_2,
                "troop_3" => $this->troop_3,
                "troop_4" => $this->troop_4,
                "troop_5" => $this->troop_5,
            ];
        return null;
    }

    public function getGoldPerHourAttribute()
    {
        if($this->village_type != "factory")
            return config('villages.resources_basic_village');
        else
            return config('villages.factory.level_bonus'.$this->village_level);
    }

    public function getTroopsPerHourAttribute()
    {
        //TIME - MINUTES
        if($this->owner_id == auth()->user()->id && $this->village_type != "blacksmith") {
            $cost = config('troops.player.' . $this->produce_troop . '.cost');
            if($this->village_type == "barracks")
                $cost *= ((100 - config('villages.barracks.level_bonus.'.$this->village_level))/100);
            return ceil(60/$cost);
        } else
            return 0;
    }

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'owner_id')->select(['id', 'username']);
    }

    public function incomingTroops()
    {
        return $this->hasMany(TroopMovement::class, 'to_tile_id', 'id')->where('has_arrived', false)->where('type', '!=', 'attack');
    }

    public function outgoingTroops()
    {
        return $this->hasMany(TroopMovement::class, 'from_tile_id', 'id')->where('has_arrived', false);
    }

    public function construction()
    {
        return $this->hasOne(Construction::class, 'village_id', 'id');
    }

    public function attacking()
    {
        return $this->hasOne(Battle::class, 'attacking_tile_id', 'id')->whereNull('winner');
    }

    public function underAttack()
    {
        return $this->hasOne(Battle::class, 'defending_tile_id', 'id')->whereNull('winner');
    }

    public function surrounded()
    {
        $x = $this->x;
        $y = $this->y;

        return $this->hasMany(MapTileSimple::class, 'owner_id', 'owner_id')->where(function($query) use ($x, $y) {
            $query->where(function($up) use ($x, $y) {
                $up->where('x', $x)->where('y', ($y - 1));
            })->orWhere(function($right) use ($x, $y) {
                $right->where('x', ($x + 1))->where('y', $y);
            })->orWhere(function($down) use ($x, $y) {
                $down->where('x', $x)->where('y', ($y + 1));
            })->orWhere(function($left) use ($x, $y) {
                $left->where('x', ($x - 1))->where('y', $y);
            });
        });
    }
}
