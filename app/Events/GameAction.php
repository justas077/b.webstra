<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GameAction implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $from_user_id;
    public $type;
    public $data;
    private $report_user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($type, $reportUser, $data)
    {
        $this->report_user_id = $reportUser;
        $this->type = $type;
        $this->data = $data;
        $this->from_user_id = auth()->user()->id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('player.'.$this->report_user_id);
    }

    public function broadcastAs()
    {
        return 'game-action';
    }
}
