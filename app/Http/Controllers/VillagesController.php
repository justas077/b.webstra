<?php

namespace App\Http\Controllers;

use App\Http\Logic\CronJobs\UpdateCron;
use App\Http\Logic\UserService;
use App\MapTile;
use App\Construction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VillagesController extends Controller
{
    /**
    *   @OA\Get(
    *       path="/api/villages/{village}",
    *       tags={"VILLAGE"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Get village",
    *       description="This route returns one village by tile id.",
    *       @OA\Parameter(
    *           name="village",
    *           description="village id (map tile id)",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           ),
    *       ),
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function get(MapTile $village)
    {
        return $village->load('incomingTroops');
    }

    /**
    *   @OA\Post(
    *       path="/api/villages/{village}/construction",
    *       tags={"VILLAGE"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Upgrade village by 1 level.",
    *       description="This route upgrades logged user's defined village. Also updates user's gold.",
    *
    *       @OA\Parameter(
    *           name="village",
    *           description="village id (map tile id)",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           ),
    *       ),
        
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function upgrade(Request $request, MapTile $village)
    {
        if($village->village_level == 0)
            $request->validate(["type" => "required|in:".implode(',', MapTile::AVAILABLE_VILLAGES)]);
        if($village->owner_id != auth()->user()->id)
            return response()->json(["status" => "error", "message" => "User is not owner of this village."], 400);

        UpdateCron::set(auth()->user(), ["troops", "gold", "constructions"]);
        $village->refresh();

        if($village->construction != null)
            return response()->json(["status" => "error", "message" => "Construction is already in process."], 400);
        if($village->village_level >= 3)
            return response()->json(["status" => "error", "message" => "Village reached maximum level."], 400);

        $upgradeCost = config('villages.'.$village->village_type.'.level_cost.'.($village->village_level + 1));
        $userGold = auth()->user()->gold;

        if($userGold < $upgradeCost)
            return response()->json(["status" => "error", "message" => "You do not have enough gold."], 400);

        auth()->user()->update(["gold" => $userGold - $upgradeCost]);

        if(is_null($village->village_type) && $village->village_level == 0) {
            $village->update(["village_type" => $request->input('type')]);
            $village->refresh();
        }

        Construction::create([
            "village_id" => $village->id,
            "construction_ends_at" => Carbon::now()->addMinutes(config('villages.'.$village->village_type.'.level_duration.'.($village->village_level + 1))),
            "type" => $village->village_level == 0 ? $request->input('type') : $village->village_type,
            "level" => ($village->village_level + 1)
        ]);

        return $village->load(['incomingTroops', 'construction']);
    }


    /**
    *   @OA\Delete(
    *       path="/api/villages/{village}",
    *       tags={"VILLAGE"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Demolish building",
    *       description="This route demolishes village to level 0, also resets it's type to null and also deletes village's construction if it exists.",
    *       
    *       @OA\Parameter(
    *           name="village",
    *           description="village ID (map tile ID)",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           ),
    *       ),
        
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function destroy(MapTile $village)
    {
        if($village->owner_id != auth()->user()->id)
            return response()->json(["status" => "error", "message" => "User is not owner of this village."], 400);

        UpdateCron::set(auth()->user(), ["troops", "gold", "constructions"]);

        if($village->construction != null)
            $village->construction->delete();

        $village->update(["village_level" => 0, "village_type" => null, "produce_troop" => "troop_1"]);

        return $village->refresh()->load('incomingTroops');
    }
}
