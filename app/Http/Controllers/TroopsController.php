<?php

namespace App\Http\Controllers;

use App\Events\GameAction;
use App\Http\Logic\CronJobs\TroopsCron;
use App\Http\Logic\CronJobs\UpdateCron;
use App\Http\Logic\CronJobs\VillageCron;
use App\Http\Logic\TroopsService;
use App\Http\Requests\Troops\Move;
use App\MapTile;
use App\TroopMovement;

class TroopsController extends Controller
{
    /**
    *   @OA\Get(
    *       path="/api/troops/movements",
    *       tags={"TROOPS"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Troop's movements",
    *       description="This route returns all logged user not finished troop's movements.",
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *   )
    */
    public function index()
    {
        UpdateCron::set(auth()->user(), ["troops", "constructions"]);
        $movements =  TroopMovement::query()->where('created_by', auth()->user()->id)->where('has_arrived', false)
            ->with(['fromRel', 'toRel'])->paginate(20);

        return $movements->setCollection($movements->getCollection()->makeVisible('to'));
//        foreach (['from', 'to'] as $w) {
//            for($i = 0; $i < count($movements["data"]); $i++) {
//                foreach (['troops', 'gold_per_hour', 'troops_per_hour', 'produce_troop'] as $el)
//                    unset($movements["data"][$i][$w][$el]);
//            }
//        }
    }

    /**
    *   @OA\Put(
    *       path="/api/troops/move/{from}/{to}",
    *       tags={"TROOPS"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Move troops",
    *       description="This route move troops between own villages.",
    *       @OA\RequestBody(
    *           description="All troop's types are required.",
    *           required=true,
    *           @OA\MediaType(
    *           mediaType="application/json",
    *               @OA\Schema(
    *                   required={"troops"},
    *                   @OA\Property(
    *                       property="troops",
    *                       description="troops - description",
    *                       type="object",
    *                       example={"troop_1" : 15, "troop_2" : 10, "troop_3" : 0},
    *                   ),
    *               )
    *           )
    *       ),
    *       
    *       @OA\Parameter(
    *           name="from",
    *           description="village (map tile) id",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           ),
    *       ),
    *       @OA\Parameter(
    *           name="to",
    *           description="village (map tile) id",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer"
    *           ),
    *       ),
    *
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function move(MapTile $from, MapTile $to, Move $request)
    {
        UpdateCron::set(auth()->user(), ["troops", "constructions"]);
        $from->refresh();
        if($from->owner_id != auth()->user()->id || $to->owner_id != auth()->user()->id)
            return response()->json(["status" => "error", "message" => "Wrong village owner."], 400);

        $service = new TroopsService();
        if(is_null(($arrive = $service->move($from, $to, $request))))
            return response()->json(["status" => "error", "message" => "Bad data."], 400);

        $to->load('incomingTroops')->refresh();
        $battle = null;
        if($to->underAttack != null)
            $battle = $to->underAttack->load(['notArrivedBackups']);

        return [
            "from" => $from->load('incomingTroops')->refresh(),
            "to" => $to,
            "battle" => $battle
        ];
    }


    /**
     *   @OA\Put(
     *       path="/api/troops/attack/{from}/{to}",
     *       tags={"TROOPS"},
     *       security={
     *           {"passport": {}},
     *       },
     *       summary="Attack village",
     *       description="This route creates attack between two villages.",
     *       @OA\RequestBody(
     *           description="All troop's types are required.",
     *           required=true,
     *           @OA\MediaType(
     *           mediaType="application/json",
     *               @OA\Schema(
     *                   required={"troops"},
     *                   @OA\Property(
     *                       property="troops",
     *                       description="troops - description",
     *                       type="object",
     *                       example={"troop_1" : 15, "troop_2" : 10, "troop_3" : 0},
     *                   ),
     *               )
     *           )
     *       ),
     *
     *       @OA\Parameter(
     *           name="from",
     *           description="village (map tile) id",
     *           required=true,
     *           in="path",
     *           @OA\Schema(
     *               type="integer"
     *           ),
     *       ),
     *       @OA\Parameter(
     *           name="to",
     *           description="village (map tile) id",
     *           required=true,
     *           in="path",
     *           @OA\Schema(
     *               type="integer"
     *           ),
     *       ),
     *
     *       @OA\Response(
     *           response=200,
     *           description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *       @OA\Response(response=404, description="Resource Not Found")
     *   )
     */
    public function attack(MapTile $from, MapTile $to, Move $request)
    {
        UpdateCron::set(auth()->user(), ["troops", "constructions"]);
        if($from->owner_id != auth()->user()->id || $to->owner_id == auth()->user()->id)
            return response()->json(["status" => "error", "message" => "Wrong village owner."], 400);

        $service = new TroopsService();
        if(($battle = ($service->attack($from, $to, $request))) == null)
            return response()->json(["status" => "error", "message" => "Bad data."], 400);

        $from->load('incomingTroops')->refresh()->makeHidden(['attacking', 'underAttack']);
        $to->load('incomingTroops')->refresh()->makeHidden(['attacking', 'underAttack']);

        return [
            "from" => $from,
            "to" => $to,
            "battle" => $battle->load(['notArrivedBackups'])
        ];
    }
}
