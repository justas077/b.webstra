<?php

namespace App\Http\Controllers;

use App\Http\Logic\Generate\MapTileService;
use App\MapTile;
use Illuminate\Http\Request;

class GenerateController extends Controller
{

    /**
    *   @OA\Get(
    *       path="/api/configuration",
    *       tags={"OTHER"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Project's configuration",
    *       description="Configuration such as: map size, troops and villages parameters...",
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *   )
    */
    public function configuration()
    {
        $villages = config('villages');
        unset($villages["resources_basic_village"]);

        $playerTroops = config('troops.player');
        unset($playerTroops["troop_4"]);
        unset($playerTroops["troop_5"]);

        return [
            "map_size" => config('parameters.map_size'),
            "map_chunk_size" => config('parameters.chunk_size'),
            "nature_troops" => config('troops.nature'),
            "player_troops" => $playerTroops,
            "villages" => $villages
        ];
    }
}
