<?php

namespace App\Http\Controllers;

use App\Http\Logic\CronJobs\TroopsCron;
use App\Http\Logic\CronJobs\UpdateCron;
use App\Http\Logic\CronJobs\VillageCron;
use App\Http\Logic\UserService;
use App\MapTile;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
    *   @OA\Get(
    *       path="/api/user/",
    *       tags={"USER"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Current user",
    *       description="This route return current logged user.",
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function get()
    {
        return auth()->user();
    }

    /**
    *   @OA\Get(
    *       path="/api/user/resources",
    *       tags={"USER"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Logged user resources",
    *       description="This route returns logged user resources, such as: gold, troops, gold per hour and etc. Also this route updates user's gold.",
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *   )
    */
    public function resources()
    {
        $villages = MapTile::query()->where('owner_id', auth()->user()->id)->get();
        $goldInfo = UserService::gold(auth()->user(), $villages);
        $troopsInfo = UserService::troops($villages);
        UpdateCron::set(auth()->user(), ["troops", "constructions"]);

        return [
            "gold" => $goldInfo["gold"],
            "gold_per_hour" => $goldInfo["gold_per_hour"],
            "troops" => $troopsInfo["troops"],
            "troops_per_hour" => $troopsInfo["troops_per_hour"]
        ];
    }
}
