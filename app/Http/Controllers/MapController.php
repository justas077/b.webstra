<?php

namespace App\Http\Controllers;

use App\Http\Logic\CronJobs\TroopsCron;
use App\Http\Logic\CronJobs\UpdateCron;
use App\Http\Logic\CronJobs\VillageCron;
use App\Http\Requests\Map\GetChunks;
use App\MapTile;
use Illuminate\Http\Request;

class MapController extends Controller
{
    /**
    *   @OA\Put(
    *       path="/api/map/chunks",
    *       tags={"MAP"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="GET map chunks",
    *       description="This route returns all map tiles from given chunk.",
    *       @OA\RequestBody(
    *           description="Chunk parameter is always required.",
    *           required=true,
    *           @OA\MediaType(
    *           mediaType="application/json",
    *               @OA\Schema(
    *                   required={"chunks"},
    *                   @OA\Property(
    *                       property="chunks",
    *                       description="chunks - description",
    *                       type="array",
    *                       example={{"x" : 1, "y" : 1}, {"x" : 1, "y" : 4}},
    *						items={"5"},
	*                   )
    *               )
    *           )
    *       ),
    *

    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *   )
    */
    public function getChunks(GetChunks $request)
    {
        $chunks = $request->input('chunks');
        $data = MapTile::query();
        $chunkSize = (int)env('CHUNK_SIZE');
        foreach ($chunks as $chunk) {
            $xRange = $chunk['x'] * $chunkSize;
            $yRange = $chunk['y'] * $chunkSize;
            $xArray = range($xRange, $xRange + $chunkSize - 1);
            $yArray = range($yRange, $yRange + $chunkSize - 1);
            $data->orWhere(function($query) use ($xArray, $yArray) {
                $query->whereIn('x', $xArray)->whereIn('y', $yArray);
            });
        }
        return $data->get();
    }

    /**
    *   @OA\Get(
    *       path="/api/map/current-user",
    *       tags={"MAP"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="Current user map tiles",
    *       description="This route returns all map tiles of logged user. Also it updates player troops.",
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *   )
    */
    public function currentUser()
    {
        UpdateCron::set(auth()->user(), ["troops", "constructions"]);
        return MapTile::query()->with(['incomingTroops'])->where('owner_id', auth()->user()->id)->orderBy('updated_at', 'desc')->get();
    }
}
