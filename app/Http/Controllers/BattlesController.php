<?php

namespace App\Http\Controllers;

use App\Battle;
use App\Http\Logic\CronJobs\BattleCron;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BattlesController extends Controller
{
    /**
    *   @OA\Get(
    *       path="/api/battles",
    *       tags={"BATTLES"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="All user's battles",
    *       description="This route returns all battles where current user is fighting.",
     *      @OA\Parameter(
     *         name="return",
     *         in="query",
     *         description="Defines what to return.",
     *         explode=true,
     *         @OA\Schema(
     *             type="string",
     *             default="all",
     *             @OA\Items(
     *                 type="string",
     *                 enum = {"not_finished", "finished", "all"},
     *             )
     *         )
     *     ),
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function index(Request $request)
    {
        BattleCron::updateBattles(auth()->user()->id);
        $battles = Battle::query()->where(function($query) {
            $query->where('created_by', auth()->user()->id)->orWhere('defender_id', auth()->user()->id);
        })->with(['notArrivedBackups']);

        $return = "all";
        if($request->filled('return')) {
            $request->validate(['return' => 'in:not_finished,finished,all']);
            $return = $request->input('return');
        }

        if($return == "not_finished")
            $battles->whereNull('winner');
        if($return == "finished")
            $battles->whereNotNull('winner');

        return $battles->get();
    }

    /**
    *   @OA\Get(
    *       path="/api/battles/{battle}",
    *       tags={"BATTLES"},
    *       security={
    *           {"passport": {}},
    *       },
    *       summary="User's battle",
    *       description="This route returns one user's battle.",
    *       @OA\Parameter(
    *           name="battle",
    *           description="battle-description",
    *           required=true,
    *           in="path",
    *           @OA\Schema(
    *               type="integer|string"
    *           ),
    *       ),
    *       @OA\Response(
    *           response=200,
    *           description="successful operation"
    *       ),
    *       @OA\Response(response=400, description="Bad request"),
    *       @OA\Response(response=404, description="Resource Not Found")
    *   )
    */
    public function get($battle)
    {

        $battle = Battle::query()->where('battle_starts_at', '<=', Carbon::now())
            ->where(function($query) use ($battle) {
                $query->where('defender_id', auth()->user()->id)->orWhere('battles.created_by', auth()->user()->id);
            })->where('battles.id', $battle)
            ->join('map_tiles', function($join) {
                $join->on('battles.defending_tile_id', '=', 'map_tiles.id');
            })->select([
                'battles.*',
                'map_tiles.troop_1 as troop_1_def',
                'map_tiles.troop_2 as troop_2_def',
                'map_tiles.troop_3 as troop_3_def',
                'map_tiles.troop_4 as troop_4_def',
                'map_tiles.troop_5 as troop_5_def',
            ])->with(['arrivedBackups'])->first();

        if($battle != null) {
            if($battle->winner == null)
                BattleCron::updateBattle($battle);

            if($battle->created_by == auth()->user()->id || $battle->defender_id == auth()->user()->id)
                return $battle->load(['notArrivedBackups'])->refresh();
        }

        return response()->json(["status" => "error", "message" => "You are not in battle."], 401);
    }
}
