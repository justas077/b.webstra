<?php

namespace App\Http\Requests\Troops;

use Illuminate\Foundation\Http\FormRequest;

class Move extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "troops" => "required",
            "troops.troop_1" => "min:0|numeric",
            "troops.troop_2" => "min:0|numeric",
            "troops.troop_3" => "min:0|numeric"
        ];
    }
}
