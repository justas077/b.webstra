<?php

namespace App\Http\Logic\Generate;


use App\MapTile;

class MapTileService
{
    const INCREASE_PERCENTAGE = 27;

    private $xSize = 0;
    private $ySize = 0;
    private $percentages;
    private $percentagesFrom;
    private $percentagesTo;
    private $amountOfFields = 0;

    public function __construct($x, $y)
    {
        $this->xSize = $x;
        $this->ySize = $y;
        $this->resetPercentages();
        $this->amountOfFields = count($this->percentages);
    }

    public function generate()
    {
        $map = [];
        for($y = 0; $y < $this->ySize; $y++) {
            for($x = 0; $x < $this->xSize; $x++) {
                $randomNumber = rand(1,100);
                $first = null;
                if(array_key_exists(($x-1).";".$y, $map)) {
                    $first = $map[($x-1).";".$y];
                    $this->increasePercentage($first);
                }

                if(array_key_exists($x.";".($y-1), $map))
                    $this->increasePercentage($map[$x.";".($y-1)], $first);

                $map[$x.";".$y] = $this->defineField($randomNumber);
                $this->resetPercentages();
            }
        }
        return $map;
    }

    private function increasePercentage($field, $first = null)
    {
        $before = 100 - $this->percentages[$field];
        $current = 100 - ($this->percentages[$field] += self::INCREASE_PERCENTAGE);
        foreach ($this->percentages as $pField => $percentage) {
            if($pField != $field && $pField != $first)
                $this->percentages[$pField] = (int)ceil($percentage * $current / $before);
        }

        $this->percentagesTo = $this->percentagesTo($this->percentages);
        $this->percentagesFrom = $this->percentagesFrom($this->percentagesTo, $this->percentages);
    }

    private function defineField($randomNumber) : string
    {
        foreach ($this->percentagesTo as $field => $percentageTo) {
            $percentageFrom = $this->percentagesFrom[$field];
            if($randomNumber >= $percentageFrom && $randomNumber <= $percentageTo)
                return $field;
        }
        return null;
    }

    private function percentagesTo($percentages)
    {
        $newPercentages = [];
        $last = 0;
        foreach ($percentages as $field => $percentage) {
            $newPercentages[$field] = ($last += $percentage);
        }
        return $newPercentages;
    }

    private function percentagesFrom($percentagesTo, $percentages)
    {
        $newPercentages = [];
        foreach ($percentagesTo as $field => $percentage) {
            $newPercentages[$field] = $percentage - $percentages[$field] + 1;
        }
        return $newPercentages;
    }

    private function resetPercentages()
    {
        $this->percentages = MapTile::AVAILABLE_TYPES_PERCENTAGES;
        $this->percentagesTo = $this->percentagesTo($this->percentages);
        $this->percentagesFrom = $this->percentagesFrom($this->percentagesTo, $this->percentages);
    }
}