<?php
namespace App\Http\Logic;


use App\Battle;
use App\Events\GameAction;
use App\Http\Logic\CronJobs\TroopsCron;
use App\Http\Requests\Troops\Move;
use App\MapTile;
use App\TroopMovement;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TroopsService
{
    public function move(MapTile $from, MapTile $to, Move $request, $isAttack = false)
    {
        if($from->x + 1 != $to->x &&
            $from->x - 1 != $to->x &&
            $from->y + 1 != $to->y &&
            $from->y - 1 != $to->y)
            return null;

        $troop1 = floor($request->input('troops.troop_1') ?? 0);
        $troop2 = floor($request->input('troops.troop_2') ?? 0);
        $troop3 = floor($request->input('troops.troop_3') ?? 0);

        if($from->troop_1 >= $troop1 &&
            $from->troop_2 >= $troop2 &&
            $from->troop_3 >= $troop3 &&
            ($troop1 > 0 || $troop2 > 0 || $troop3 > 0)) {

            $type = "backup";
            if($isAttack)
                if($from->attacking != null || $to->underAttack != null)
                    $type = "attack_backup";
                else
                    $type = "attack";

            DB::beginTransaction();
            $from->update([
                "troop_1" => DB::raw('troop_1-'.$troop1),
                "troop_2" => DB::raw('troop_2-'.$troop2),
                "troop_3" => DB::raw('troop_3-'.$troop3)
            ]);

            $movements = TroopMovement::create([
                'from_tile_id' => $from->id,
                'to_tile_id' => $to->id,
                'troop_1' => $troop1,
                'troop_2' => $troop2,
                'troop_3' => $troop3,
                'will_arrive_at' => Carbon::now()->addMinutes(config('troops.troop_speed')),
                'type' => $type
            ])->fresh();
            DB::commit();

            if($type == "backup" && $to->underAttack != null)
                event(new GameAction("backup", $to->underAttack->created_by, [
                    "battle" => $to->underAttack->load('notArrivedBackups')
                ]));

            return $movements;
        }

        DB::rollBack();
        return null;
    }


    public function attack(MapTile $from, MapTile $to, Move $request)
    {
        $battle = null;
        if($this->move($from, $to, $request, true)) {
            $from->refresh(); $to->refresh();
            if($from->attacking == null && $to->underAttack == null) {
                $troop1 = (int)floor($request->input('troops.troop_1') ?? 0);
                $troop2 = (int)floor($request->input('troops.troop_2') ?? 0);
                $troop3 = (int)floor($request->input('troops.troop_3') ?? 0);

                $battle = Battle::create([
                    'attacking_tile_id' => $from->id,
                    'defending_tile_id' => $to->id,
                    'troop_1_atk' => $troop1,
                    'troop_2_atk' => $troop2,
                    'troop_3_atk' => $troop3,
                    'defender_id' => $to->owner_id,
                    'next_stage_at' => Carbon::now()->addMinutes(config('troops.troop_speed')),
                    'battle_starts_at' => Carbon::now()->addMinutes(config('troops.troop_speed'))
                ])->fresh();

                if($battle->defender_id != 2) {
                    TroopsCron::updateVillageTroops($battle->defender_id);
                    event(new GameAction("attack", $battle->defender_id, [
                        "from_village" => $from = ($from->owner_id == auth()->user()->id ? $from : $from->makeHidden(['troops', 'produce_troop', 'troops_at', 'gold_per_hour', 'troops_per_hour', 'construction'])),
                        "to_village" => $to = ($to->owner_id == auth()->user()->id ? $to : $to->makeHidden(['troops', 'produce_troop', 'troops_at', 'gold_per_hour', 'troops_per_hour', 'construction'])),
                        "battle" => $battle->load('notArrivedBackups')
                    ]));
                }
            } else {
                event(new GameAction("attack-backup", $to->owner_id, [
                    "battle" => $to->underAttack->load('notArrivedBackups')
                ]));
                $battle = $to->underAttack;
            }
        }

        return $battle;
    }


}