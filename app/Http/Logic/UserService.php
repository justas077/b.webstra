<?php
namespace App\Http\Logic;


use App\MapTile;
use App\User;
use Carbon\Carbon;

class UserService
{
    public static function gold($user, $villages)
    {
        if($user == null)
            $user = auth()->user();
        if($villages == null)
            $villages = MapTile::query()->where('owner_id', $user->id)->get();
        $goldPerHour = 0;
        foreach ($villages as $village) {
            if($village->village_type != "factory")
                $goldPerHour += config('villages.resources_basic_village');
            else
                $goldPerHour += config('villages.factory.level_bonus.'.$village->village_level);
        }

        $updated = new Carbon(auth()->user()->updated_at);
        $seconds = $updated->diffInSeconds(Carbon::now());
        $gold = $user->gold + ($goldPerHour/3600) * $seconds;
        $user->update([
            "gold" => $gold,
            "resources_at" => now()
        ]);

        return [
            "gold" => (int)floor($gold),
            "gold_per_hour" => $goldPerHour,
        ];
    }

    public static function troops($villages)
    {
        $troopsPerHour = [
            "troop_1" => 0,
            "troop_2" => 0,
            "troop_3" => 0
        ];
        $troops = [
            "troop_1" => 0,
            "troop_2" => 0,
            "troop_3" => 0
        ];
        foreach ($villages as $village) {
            $owner = $village->owner_id == 2 ? "nature" : "player";
            $cost = config('troops.' . $owner . '.' . $village->produce_troop . '.cost');
            if($village->village_type == "barracks")
                $cost *= ((100 - config('villages.barracks.level_bonus.'.$village->village_level))/100);
            if($village->village_type != "blacksmith")
                $troopsPerHour[$village->produce_troop] += ceil(60/$cost);

            $troops["troop_1"] += (int)floor($village->troop_1);
            $troops["troop_2"] += (int)floor($village->troop_2);
            $troops["troop_3"] += (int)floor($village->troop_3);
        }

        return [
            "troops" => $troops,
            "troops_per_hour" => $troopsPerHour
        ];
    }
}