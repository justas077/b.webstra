<?php

namespace App\Http\Logic\CronJobs;


use App\Battle;
use App\Construction;
use App\MapTile;
use Carbon\Carbon;

class BattleCron
{
    public static function updateBattle(Battle $battle)
    {
        $stageDuration = config('battles.one_stage_duration_min');
        $winner = null;
        $stagesCount = (int)floor(Carbon::now()->diffInMinutes(new Carbon($battle->next_stage_at)) / config('battles.one_stage_duration_min'));
        if ($stagesCount < 1)
            return;
        $troopsParameters = self::firstStage($battle, ($battle->defender_id == 2));
        $stageTime = new Carbon($battle->next_stage_at);
        for ($i = $battle->stage; $i < ($battle->stage + $stagesCount); $i++) {
            if ($troopsParameters["status"] >= 100) {
                $troopsParameters["winner"] = "attacker";
                break;
            }

            $start = new Carbon($stageTime);
            $end = $stageTime->addMinutes($stageDuration);
            $defenderBackups = $battle->arrivedBackups()->where('type', 'backup')->whereBetween('will_arrive_at', [$start, $end]);
            $attackerBackups = $battle->arrivedBackups()->where('type', 'attack_backup')->whereBetween('will_arrive_at', [$start, $end]);

            if (!$defenderBackups->get()->isEmpty() || !$attackerBackups->get()->isEmpty()) {
                $troopsParameters = self::addBackups($troopsParameters, $attackerBackups->get(), $defenderBackups->get());
                $attackerBackups->update(["has_arrived" => true]);
                $defenderBackups->update(["has_arrived" => true]);
            }

            if ($troopsParameters["winner"] == null) {
                $troopsParameters = self::stageBattle($troopsParameters, true);
            } else
                break;
        }

        $nextStage = (new Carbon($battle->next_stage_at))->addMinutes($stagesCount * $stageDuration);
        if ($troopsParameters["winner"] == "attacker") {
            $defendingTileUpdate = [
                "troop_1" => $troopsParameters["troop_1_atk"],
                "troop_2" => $troopsParameters["troop_2_atk"],
                "troop_3" => $troopsParameters["troop_3_atk"],
                "troop_4" => 0,
                "troop_5" => 0,
                "village_type" => null,
                "village_level" => 0,
                "produce_troop" => "troop_1",
                "troops_at" => Carbon::now(),
                "owner_id" => $battle->created_by
            ];

            foreach ($battle->arrivedBackups()->where('type', 'backup') as $defBackup)
                $defBackup->update(["to_tile_id" => $defBackup->from_tile_id]);
            $battle->arrivedBackups()->where('type', 'attack_backup')->update(["type" => "backup"]);
            Construction::query()->where('village_id', $battle->defending_tile_id)->delete();
        } else {
            $defendingTileUpdate = [
                "troop_1" => $troopsParameters["troop_1_def"],
                "troop_2" => $troopsParameters["troop_2_def"],
                "troop_3" => $troopsParameters["troop_3_def"],
                "troop_4" => $troopsParameters["troop_4_def"],
                "troop_5" => $troopsParameters["troop_5_def"]
            ];
            if ($troopsParameters["winner"] == "defender")
                foreach ($battle->arrivedBackups()->where('type', 'attack_backup') as $atkBackup)
                    $atkBackup->update(["to_tile_id" => $atkBackup->from_tile_id]);
        }

        $battle->defender->update($defendingTileUpdate);
        $battle->update([
            "stage" => ($battle->stage + $stagesCount),
            "winner" => $troopsParameters["winner"],
            "status" => ($troopsParameters["status"] >= 100) ? 100 : $troopsParameters["status"],
            "troop_1_atk" => $troopsParameters["troop_1_atk"],
            "troop_2_atk" => $troopsParameters["troop_2_atk"],
            "troop_3_atk" => $troopsParameters["troop_3_atk"],
            "next_stage_at" => $nextStage
        ]);

        if ((new Carbon($battle->battle_starts_at)) <= Carbon::now())
            $battle->battleMovement->update(["has_arrived" => true]);
    }


    public static function updateBattles($userId)
    {
        $battles = Battle::query()->whereNull('winner')->where('battle_starts_at', '<=', Carbon::now())
            ->where(function($query) use ($userId) {
                $query->where('defender_id', $userId)->orWhere('battles.created_by', $userId);
            })
            ->join('map_tiles', function($join) {
                $join->on('battles.defending_tile_id', '=', 'map_tiles.id');
            })->select([
                'battles.*',
                'map_tiles.troop_1 as troop_1_def',
                'map_tiles.troop_2 as troop_2_def',
                'map_tiles.troop_3 as troop_3_def',
                'map_tiles.troop_4 as troop_4_def',
                'map_tiles.troop_5 as troop_5_def',
            ])->with(['arrivedBackups']);

        foreach ($battles->get() as $battle) {
            self::updateBattle($battle);
        }
    }


    private static function firstStage($battle, bool $nature)
    {
        $defender = $nature ? "nature" : "player";

        $attackingTroopsCount = $battle->troop_1_atk + $battle->troop_2_atk + $battle->troop_3_atk;
        $defendingTroopsCount = $battle->troop_1_def + $battle->troop_2_def + $battle->troop_3_def + $battle->troop_4_def + $battle->troop_5_def;

        $attackingTroopsPower = 0;
        $attackingTroopsHP = 0;
        for($i = 1; $i <= 3; $i++) {
            $troop = "troop_".$i."_atk";
            $attackingTroopsPower += config('troops.player.troop_'. $i .'.power') * $battle->{$troop};
            $attackingTroopsHP += config('troops.player.troop_'. $i .'.hp') * $battle->{$troop};
        }

        $defendingTroopsPower = 0;
        $defendingTroopsHP = 0;
        for($i = 1; $i <= 5; $i++) {
            $troop = "troop_".$i."_def";
            $defendingTroopsPower += config('troops.'. $defender .'.troop_'. $i .'.power') * $battle->{$troop};
            $defendingTroopsHP += config('troops.'. $defender .'.troop_'. $i .'.hp') * $battle->{$troop};
        }

        return [
            "attacking_troops_count" => $attackingTroopsCount,
            "defending_troops_count" => $defendingTroopsCount,
            "total_troops_count" => ($attackingTroopsCount + $defendingTroopsCount),

            "attacking_troops_power" => $attackingTroopsPower,
            "defending_troops_power" => $defendingTroopsPower,
            "total_troops_power" => ($attackingTroopsPower + $defendingTroopsPower),

            "attacking_troops_hp" => $attackingTroopsHP,
            "defending_troops_hp" => $defendingTroopsHP,
            "total_troops_hp" => ($attackingTroopsHP + $defendingTroopsHP),

            "status" => $battle->status,

            "troop_1_atk" => $battle->troop_1_atk,
            "troop_2_atk" => $battle->troop_2_atk,
            "troop_3_atk" => $battle->troop_3_atk,

            "troop_1_def" => $battle->troop_1_def,
            "troop_2_def" => $battle->troop_2_def,
            "troop_3_def" => $battle->troop_3_def,
            "troop_4_def" => $battle->troop_4_def,
            "troop_5_def" => $battle->troop_5_def,

            "winner" => null
        ];
    }

    private static function stageBattle($parameters, bool $nature)
    {
        $defender = $nature ? "nature" : "player";

        //FIRST STAGE - SLIDER PUSH
        $attackingTroopsPercentage = $parameters["attacking_troops_power"] / $parameters["total_troops_power"];
        $pushDivider = config('battles.one_stage_slider_divider');
        if($attackingTroopsPercentage <= config('battles.attack_effectiveness_drops_at'))
            $pushDivider *= config('battles.attack_penalty_multiply');

        $pushBy = $attackingTroopsPercentage / $pushDivider;
        $status = $parameters["status"] + $pushBy;


        //SECOND STAGE - TROOP DEATH
        $defendingTroopsPercentage = ($parameters["defending_troops_hp"] / $parameters["total_troops_hp"]) + 0.001; //also percentage of attacker troops witch should die

        $shouldDie = $parameters["total_troops_count"] * config('battles.die_in_one_stage');
        $attackingShouldDie = $shouldDie * $defendingTroopsPercentage;
        $defendingShouldDie = $shouldDie - $attackingShouldDie;

        if($attackingShouldDie < 0.5 && $defendingShouldDie < 0.5)
            $attackingShouldDie = 1;

        // troops are dieing randomly
        $attackingDie = round($attackingShouldDie);
        for($i = 0; $i < $attackingDie; $i++) {
            if(!empty($availableAtk = (self::availableTroopReduce($parameters, "atk", 3)))) {
                $atkTroop = $availableAtk[array_rand($availableAtk)];
                $parameters["troop_". $atkTroop ."_atk"] -= 1;
                $parameters["attacking_troops_power"] -= config('troops.player.troop_'.$atkTroop.'.power');
                $parameters["attacking_troops_hp"] -= config('troops.player.troop_'.$atkTroop.'.hp');
            } else {
                $parameters["winner"] = "defender";
                return $parameters;
            }
        }


        $defendingDie = round($defendingShouldDie);
        for($i = 0; $i < $defendingDie; $i++) {
            if(!empty($availableDef = (self::availableTroopReduce($parameters, "def", 5)))) {
                $defTroop = $availableDef[array_rand($availableDef)];
                $parameters["troop_". $defTroop ."_def"] -= 1;
                $parameters["defending_troops_power"] -= config('troops.'. $defender .'.troop_'.$defTroop.'.power');
                $parameters["defending_troops_hp"] -= config('troops.'. $defender .'.troop_'.$defTroop.'.hp');
            } else {
                $parameters["winner"] = "attacker";
                return $parameters;
            }
        }

        return [
            "attacking_troops_count" => $parameters["attacking_troops_count"] - $attackingDie,
            "defending_troops_count" => $parameters["defending_troops_count"] - $defendingDie,
            "total_troops_count" => $parameters["total_troops_count"] - ($attackingDie + $defendingDie),

            "attacking_troops_power" => $parameters["attacking_troops_power"],
            "defending_troops_power" => $parameters["defending_troops_power"],
            "total_troops_power" => ($parameters["attacking_troops_power"] + $parameters["defending_troops_power"]),

            "attacking_troops_hp" => $parameters["attacking_troops_hp"],
            "defending_troops_hp" => $parameters["defending_troops_hp"],
            "total_troops_hp" => ($parameters["attacking_troops_hp"] + $parameters["defending_troops_hp"]),

            "status" => $status,

            "troop_1_atk" => $parameters["troop_1_atk"],
            "troop_2_atk" => $parameters["troop_2_atk"],
            "troop_3_atk" => $parameters["troop_3_atk"],

            "troop_1_def" => $parameters["troop_1_def"],
            "troop_2_def" => $parameters["troop_2_def"],
            "troop_3_def" => $parameters["troop_3_def"],
            "troop_4_def" => $parameters["troop_4_def"],
            "troop_5_def" => $parameters["troop_5_def"],
            "winner" => null
        ];
    }

    private static function availableTroopReduce($parameters, $type, $amount)
    {
        $available = [];

        for($i = 1; $i <= $amount; $i++)
            if($parameters["troop_".$i."_".$type] > 0)
                $available[] = $i;

        return $available;
    }





    // ****NEEDS REFACTOR****
    private static function addBackups($parameters, $attackerBackups, $defenderBackups)
    {
        $attackingTroopsCount = 0;
        $defendingTroopsCount = 0;
        foreach ($attackerBackups as $attackerBackup) {
            $attackingTroopsCount += ($attackerBackup->troop_1 + $attackerBackup->troop_2 + $attackerBackup->troop_3);

            $parameters["troop_1_atk"] += $attackerBackup->troop_1;
            $parameters["troop_2_atk"] += $attackerBackup->troop_2;
            $parameters["troop_3_atk"] += $attackerBackup->troop_3;

            $parameters["attacking_troops_power"] += (
                $attackerBackup->troop_1 * config('troops.player.troop_1.power') +
                $attackerBackup->troop_2 * config('troops.player.troop_2.power') +
                $attackerBackup->troop_3 * config('troops.player.troop_3.power')
            );

            $parameters["attacking_troops_hp"] += (
                $attackerBackup->troop_1 * config('troops.player.troop_1.hp') +
                $attackerBackup->troop_2 * config('troops.player.troop_2.hp') +
                $attackerBackup->troop_3 * config('troops.player.troop_3.hp')
            );
        }

        foreach ($defenderBackups as $defenderBackup) {
            $defendingTroopsCount += ($defenderBackup->troop_1 + $defenderBackup->troop_2 + $defenderBackup->troop_3);

            $parameters["troop_1_def"] += $defenderBackup->troop_1;
            $parameters["troop_2_def"] += $defenderBackup->troop_2;
            $parameters["troop_3_def"] += $defenderBackup->troop_3;

            $parameters["defending_troops_power"] += (
                $defenderBackup->troop_1 * config('troops.player.troop_1.power') +
                $defenderBackup->troop_2 * config('troops.player.troop_2.power') +
                $defenderBackup->troop_3 * config('troops.player.troop_3.power')
            );

            $parameters["defending_troops_hp"] += (
                $defenderBackup->troop_1 * config('troops.player.troop_1.power') +
                $defenderBackup->troop_2 * config('troops.player.troop_2.power') +
                $defenderBackup->troop_3 * config('troops.player.troop_3.power')
            );
        }

        $parameters["attacking_troops_count"] = $parameters["attacking_troops_count"] + $attackingTroopsCount;
        $parameters["defending_troops_count"] = $parameters["defending_troops_count"] + $defendingTroopsCount;
        $parameters["total_troops_count"] = $parameters["total_troops_count"] + ($attackingTroopsCount + $defendingTroopsCount);
        $parameters["total_troops_power"] = ($parameters["attacking_troops_power"] + $parameters["defending_troops_power"]);
        $parameters["total_troops_hp"] = ($parameters["attacking_troops_hp"] + $parameters["defending_troops_hp"]);

        return $parameters;
    }

}