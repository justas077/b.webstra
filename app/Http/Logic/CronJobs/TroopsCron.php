<?php

namespace App\Http\Logic\CronJobs;


use App\Battle;
use App\Http\Controllers\TroopsController;
use App\MapTile;
use App\TroopMovement;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TroopsCron
{

    public static function updateVillageTroops($userId)
    {
        $tiles = MapTile::query()->with(['construction', 'underAttack'])->where('owner_id', $userId)->get();
        $update = [
            "troop_1" => [],
            "troop_2" => [],
            "troop_3" => [],
            "troop_4" => [],
            "troop_5" => [],
            "ids_troop_1" => [],
            "ids_troop_2" => [],
            "ids_troop_3" => [],
            "ids_troop_4" => [],
            "ids_troop_5" => []
        ];

        foreach ($tiles as $tile) {
            if($tile->village_type != "blacksmith" && $tile->underAttack == null) {
                $updated = new Carbon($tile->troops_at);
                $seconds = $updated->diffInSeconds(Carbon::now());
                $troopCost = $cost = config('troops.player.' . $tile->produce_troop . '.cost');
                if($tile->village_type == "barracks")
                    $troopCost *= ((100 - config('villages.barracks.level_bonus.'.$tile->village_level))/100);
                $increase = ($seconds/60)/$troopCost;
                $sum = $tile->{$tile->produce_troop} + $increase;

                //if barrack construction in progress
                if($tile->village_type == "barracks" && !is_null($tile->construction) && $tile->construction->construction_ends_at <= Carbon::now()) {
                    $ends = new Carbon($tile->construction->construction_ends_at);
                    $constructSeconds = $ends->diffInSeconds(Carbon::now());
                    $levelBeforeTroops = ($constructSeconds/60) / $troopCost;
                    $levelAfterTroops = ($constructSeconds/60) / ($cost * ((100 - config('villages.barracks.level_bonus.'.($tile->village_level + 1)))/100));
                    $sum += ($levelAfterTroops - $levelBeforeTroops);
                }

                $update[$tile->produce_troop][] = "when id = {$tile->id} then {$sum}";
                $idsName = "ids_".$tile->produce_troop;
                $update[$idsName][] = $tile->id;
            }
        }

        for($i = 1; $i <= 5; $i++) {
            $updateName = "troop_".$i;
            if(!empty($update[$updateName])) {
                $whenThen = "CASE ".implode(' ', $update[$updateName])." END";
                $ids = implode(',', $update["ids_".$updateName]);
                DB::select("UPDATE map_tiles SET troops_at = now(), {$updateName} = ({$whenThen}) WHERE id IN ({$ids})");
            }
        }

        self::updateMovements($userId);
    }


    public static function updateMovements($userId)
    {
        $troopMovement = [
            "troop_1" => [],
            "troop_2" => [],
            "troop_3" => []
        ];

        $ids = [
            "ids_1" => [],
            "ids_2" => [],
            "ids_3" => []
        ];

        $trMovement = TroopMovement::query()->where('created_by', $userId)
            ->where('will_arrive_at', '<=', Carbon::now())
            ->where('has_arrived', false)
            ->where('type', 'backup');

        foreach ($trMovement->get() as $movement) {
            if($movement->toRel->underAttack == null) {
                for($i = 1; $i <= 3; $i++) {
                    if($movement->{'troop_'.$i} > 0) {
                        if(!array_key_exists($movement->to_tile_id, $troopMovement['troop_'.$i])) {
                            $troopMovement['troop_'.$i][$movement->to_tile_id] = $movement->{'troop_'.$i};
                            $ids["ids_".$i][] = $movement->to_tile_id;
                        } else
                            $troopMovement['troop_'.$i][$movement->to_tile_id] += $movement->{'troop_'.$i};
                    }
                }
            }
        }

        $trMovement->update(['has_arrived' => true]);

        $updateTroopMovement = [
            "troop_1" => [],
            "troop_2" => [],
            "troop_3" => []
        ];
        for($i = 1; $i <= 3; $i++) {
            foreach ($troopMovement["troop_".$i] as $villageId => $troopCount) {
                $then = "troop_".$i."+".$troopCount;
                $updateTroopMovement["troop_".$i][] = "when id = {$villageId} then {$then}";
            }
        }

        for($i = 1; $i <= 3; $i++) {
            if(!empty($updateTroopMovement["troop_".$i])) {
                $updateIds = implode(',', $ids["ids_".$i]);
                $whenThen = "CASE ".implode(' ', $updateTroopMovement["troop_".$i])." END";
                $updateName = "troop_".$i;
                DB::select("UPDATE map_tiles SET {$updateName} = ({$whenThen}) WHERE id IN ({$updateIds})");
            }
        }
    }
}