<?php

namespace App\Http\Logic\CronJobs;


use App\Construction;
use App\MapTile;
use App\User;
use Carbon\Carbon;

class VillageCron
{
    public static function updateConstructions($userId)
    {
        $constructions = Construction::query()->where('created_by', $userId)
            ->where('construction_ends_at', '<=', now());

        self::updateFactoryConstructions($userId);

        $villagesIds = $constructions->pluck('village_id')->toArray();
        $villages = MapTile::query()->whereIn('id', $villagesIds);
        $villages->increment('village_level');
        $constructions->delete();
    }

    private static function updateFactoryConstructions($userId)
    {
        $constructions = Construction::query()->where('created_by', $userId)
            ->where('construction_ends_at', '<=', now())
            ->where('type','factory');

        $updateGold = 0;
        foreach ($constructions->get() as $construction) {
            $ends = new Carbon($construction->construction_ends_at);
            $diffSec = $ends->diffInSeconds(Carbon::now());
            $goldDiff = config('villages.factory.level_bonus.'.$construction->level) - config('villages.factory.level_bonus.'.($construction->level - 1));
            $gold = $goldDiff / 3600 * $diffSec;
            $updateGold += $gold;
        }
        if($constructions->count() > 0)
            auth()->user()->update([
                "gold" => auth()->user()->gold + $updateGold
            ]);
    }
}