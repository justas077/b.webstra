<?php
/**
 * Created by PhpStorm.
 * User: Justas
 * Date: 4/10/2019
 * Time: 2:25 PM
 */

namespace App\Http\Logic\CronJobs;


use App\Http\Logic\UserService;
use App\User;

class UpdateCron
{
    public static function set($user, array $update)
    {
        if(in_array("troops", $update))
            TroopsCron::updateVillageTroops($user->id);
        if(in_array("gold", $update))
            UserService::gold($user, null);
        if(in_array("constructions", $update))
            VillageCron::updateConstructions($user->id);
    }
}