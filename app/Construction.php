<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    use BootableTrait;

    protected $fillable = [
        "village_id",
        "construction_ends_at",
        "type",
        "level"
    ];

    protected $hidden = [
        "created_by",
        "updated_by",
        "updated_at"
    ];
}
