<?php

namespace App\Console\Commands;

use App\MapTile;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateVillages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-villages {user} {--size=} {--destroy=} {--x=} {--y=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for villages generation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $user = $this->argument('user');
        $size = $this->option('size');
        $optX = $this->option('x');
        $optY = $this->option('y');
        $isDestroy = $this->option('destroy') == null ? false : true;
        $user = User::find($user);
        if(is_null($user) || is_null($size) || in_array($user->id, [1,2]) || $size < 4 || $size > 36) {
            $this->error("No user or size.");
            return;
        }

        MapTile::query()->where('owner_id', $user->id)->update([
            "village_type" => null,
            "updated_at" => $now,
            "troop_1" => rand(1,50),
            "troop_2" => rand(1,50),
            "troop_3" => rand(1,50),
            "troop_4" => rand(1,50),
            "troop_5" => rand(1,50),
            "produce_troop" => null,
            "owner_id" => 2
        ]);

        if(!$isDestroy) {
            $box = floor(sqrt($size));
            $x = $optX == null ? rand(env('CHUNK_SIZE'), env('MAP_SIZE') - env('CHUNK_SIZE')) : $optX;
            $y = $optY == null ? rand(env('CHUNK_SIZE'), env('MAP_SIZE') - env('CHUNK_SIZE')) : $optY;
            $this->info("x". $x. " y".$y);

            $tiles = null;
            while(is_null($tiles)) {
                $tiles = MapTile::query()->whereIn('x', range($x, $x + $box - 1))
                    ->whereIn('y', range($y, $y + $box - 1))
                    ->where('owner_id', 2);
                if($tiles->count() != $box * $box) {
                    $tiles = null;
                    $this->error("Error while trying to find free spot");
                }
            }

            foreach ($tiles->get() as $tile) {
                $tile->owner_id = $user->id;
                $tile->troop_1 = 0;
                $tile->troop_2 = 0;
                $tile->troop_3 = 0;
                $tile->troop_4 = 0;
                $tile->troop_5 = 0;
                $tile->village_type = MapTile::AVAILABLE_VILLAGES[array_rand(MapTile::AVAILABLE_VILLAGES)];
                $tile->village_level = 1;
                $tile->updated_at = $now;
                $tile->produce_troop = "troop_1";
                $tile->troops_at = Carbon::now();
                $tile->save();
            }
        }


        $this->info("success");
    }
}
