<?php

namespace App\Console\Commands;

use App\Http\Logic\Generate\MapTileService;
use App\MapTile;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-map {size}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for map generation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(60*30);
        $troopsArray = [0,0,0,0,0,0,0,0,1,2,2,3,4,4,5,6,6,7,8,8,9,10,10,10,20,20,20,30,30,40,40,50,0,0,0,0,0,0,0,0];
        try {
            $size = explode("x",$this->argument('size'));
            $xSize = $size[0];
            $ySize = $size[1];

            $service = new MapTileService($xSize, $ySize);
            $insert = [];
            $now = Carbon::now();
            foreach ($service->generate() as $coo => $tile) {
                $coo = explode(";", $coo);
                $insert[] = [
                    "x" => $coo[0],
                    "y" => $coo[1],
                    "type" => $tile,
                    "troop_1" => $troopsArray[array_rand($troopsArray)],
                    "troop_2" => $troopsArray[array_rand($troopsArray)],
                    "troop_3" => $troopsArray[array_rand($troopsArray)],
                    "troop_4" => $troopsArray[array_rand($troopsArray)],
                    "troop_5" => $troopsArray[array_rand($troopsArray)],
                    "created_at" => $now,
                    "updated_at" => $now
                ];
            }
            MapTile::query()->truncate();
            foreach (array_chunk($insert, ceil(count($insert) / 50)) as $ins) {
                MapTile::insert($ins);
            }

            $this->info("Success");
        } catch (\Exception $exception) {
            dd($exception);
        }

    }

}
