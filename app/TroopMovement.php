<?php

namespace App;

use App\Logic\Traits\BootableTrait;
use Illuminate\Database\Eloquent\Model;

class TroopMovement extends Model
{
    use BootableTrait;

    protected $fillable = [
        'from_tile_id',
        'to_tile_id',
        'troop_1',
        'troop_2',
        'troop_3',
        'will_arrive_at',
        'has_arrived',
        'type'
    ];

    protected $hidden = [
        'id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'from_tile_id',
        'to_tile_id',
        'has_arrived',
        'troop_1',
        'troop_2',
        'troop_3',
        'toRel',
        'fromRel',
        'to'
    ];

    protected $casts = [
        'has_arrived' => 'boolean'
    ];

    protected $appends = [
        'troops',
        'from',
        'to'
    ];

    public function fromRel()
    {
        return $this->hasOne(MapTile::class, 'id', 'from_tile_id')->without(['owner'])->select(['id', 'x', 'y', 'village_type', 'village_level', 'produce_troop']);
    }

    public function toRel()
    {
        return $this->hasOne(MapTile::class, 'id', 'to_tile_id')->without(['owner'])->select(['id', 'x', 'y', 'village_type', 'village_level', 'produce_troop']);
    }

    public function getToAttribute()
    {
        return [
            "x" => $this->toRel->x,
            "y" => $this->toRel->y
        ];
    }

    public function getFromAttribute()
    {
        return [
            "x" => $this->fromRel->x,
            "y" => $this->fromRel->y
        ];
    }

    public function getTroopsAttribute()
    {
        return [
            "troop_1" => $this->troop_1,
            "troop_2" => $this->troop_2,
            "troop_3" => $this->troop_3,
        ];
    }

}
